(ns gail.interceptors)

; function to create interceptor that adds openai headers
(defn add-openai-headers
  "return map with settings applied"
  ([api_key api_org]
   {:name ::add-openai-headers
    :enter (fn [ctx]
             (-> ctx
                 (assoc-in [:request :headers "Authorization"]
                           (str "Bearer " api_key))
                 (assoc-in [:request :headers "OpenAI-Organization"]
                           (str api_org))))}))
; interceptor to tap the request
(def tap-request
  {:name ::tap-request
   :enter (fn [{:keys [request] :as ctx}]
            (tap> {:from ::tap-request :request request})
            ctx)})

; interceptor to tap the response
(def tap-response
  {:name ::tap-response
   :leave (fn [{:keys [response] :as ctx}]
            (tap> {:from ::tap-response :response response})
            ctx)})
