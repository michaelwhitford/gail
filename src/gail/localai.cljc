(ns gail.localai
  (:require [babashka.http-client :as http]
            [babashka.http-client.interceptors :as i]
            [babashka.json :as json]
            [babashka.fs :as fs]
            [portal.api :as p]
            [clojure.string :as str]
            [clojure.pprint :refer [pprint]]
            [martian.core :refer [explore response-for request-for] :as martian]
            [martian.babashka.http-client :as martian-http]
            [gail.interceptors :refer [add-openai-headers tap-request tap-response]]))

; warn on reflection
(set! *warn-on-reflection* true)

(def ^:dynamic *openapi-file* (str (fs/parent *file*) "/localai.yml"))

; generate the api client from openapi file
(defn create-api-client
  "return martian api client
   Note: openapi-file ties this file to this namespace"
  ([data]
   (let [api_base_url (data "api_base_url")
         api_key (data "api_key")
         api_org (data "api_org")]
     (tap> {:from :create-api-client :url api_base_url :key api_key :org api_org})
     (martian-http/bootstrap-openapi
      *openapi-file*
      #_(martian.yaml/json->edn (slurp *openapi-file*))
      {:server-url   api_base_url
       :interceptors (concat
                      [tap-response]
                      [(add-openai-headers api_key api_org)]
                      martian-http/default-interceptors
                      [tap-request])}))))

(defn chat-completion
  "Generate chat-completion for a query.
   Params:
   - prompt: The text prompt to send
   Returns:
   - map of the query response"
  ([env {:keys [config provider] :as data}]
   (let [out      {:model               (data :model)
                   :temperature         (data :temperature)
                   :max_tokens          (data :max_tokens)
                   :seed                (data :seed)
                   :messages            [{:role    "user"
                                          :content (data :prompt_final)}]}
         response (response-for (data :client) :chat-completions {:body out})
         status   (:status response)]
     (when (= status 200)
       (tap> {:from :chat-completion :env env :data data :out out})
       response))))

(defn completion
  "Generate prompt completion for a query.
  Params:
   - prompt: The text prompt to send
   Returns:
   - map of the query response"
  ([data] (completion nil data))
  ([_ data]
   (let [out {:model       (data :model)
              :temperature (get-in data [:config "temperature"])
              :max_tokens  (get-in data [:config "max_tokens"])
              :prompt      (get-in data [:opts :queries])}
         response (response-for (:client data) :completions {:body out})
         status (:status response)]
     (when (= status 200)
       (tap> {:from :completion :data data :response response})
       response))))

(defn model-list
  "Get the available model list
   Returns:
   - A vector of available model names."
  ([data] (model-list nil data))
  ([_ data] (let [response (response-for (:client data) :models)
                  status   (response :status)]
              (when (= status 200)
                (mapv :id (get-in response [:body :data]))))))

(defn current-model
  "Get the currently loaded model - not used for localai
   Returns:
   - A string of the currently loaded model name"
  ([data] (current-model nil data))
  ([_ data] "None"))

(defn current-model?
  "Check if the current model matches the supplied model. - not used for localai
   Params:
   - model: The model name to check against the current model.
   Returns:
   - true if the current model matches the supplied model, false otherwise."
  ([model data] true))

(defn load-model!
  "Load a model. - not used for localai
   Params:
   - model: The model name to load.
   Returns:
   - The response body if the model was loaded successfully."
  ([model data] {:status 200}))

(defn unload-model!
  "Unload current model. - not used for localai
   Returns:
  - The response body."
  ([data] {:status 200}))

(comment
  (def p (p/open))
  (add-tap #'p/submit)
  (let [m (create-api-client {"api_base_url" "http://localhost:5000"
                              "api_key" "localai-key"
                              "api_org" "localai-org"})]
    (tap> m)
    (explore m :chat-completions))
  (pprint *openapi-file*)
  (fs/exists? *openapi-file*))
