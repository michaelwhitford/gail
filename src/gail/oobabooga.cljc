(ns gail.oobabooga
  (:require [babashka.http-client :as http]
            [babashka.http-client.interceptors :as i]
            [babashka.json :as json]
            [babashka.fs :as fs]
            [portal.api :as p]
            [clojure.string :as str]
            [clojure.pprint :refer [pprint]]
            [martian.core :refer [explore response-for request-for] :as martian]
            [martian.babashka.http-client :as martian-http]
            [gail.interceptors :refer [add-openai-headers tap-request tap-response]]))

; warn on reflection
(set! *warn-on-reflection* true)

(def ^:dynamic *openapi-file* (str (fs/parent *file*) "/oobabooga.json"))

; generate the api client from openapi file
(defn create-api-client
  "return martian api client
   Note: *openapi-file* ties this file to this namespace"
  ([data]
   (let [api_base_url (data "api_base_url")
         api_key (data "api_key")
         api_org (data "api_org")]
     (tap> {:from :create-api-client :data data :file *openapi-file*})
     (martian-http/bootstrap-openapi
      *openapi-file*
      {:server-url   api_base_url
       :interceptors (concat
                      [tap-response]
                      [(add-openai-headers api_key api_org)]
                      martian-http/default-interceptors
                      [tap-request])}))))

(defn chat-completion
  "Generate chat-completion for a query.
   Params:
   - prompt: The text prompt to send
   Returns:
   - map of the query response"
  ([env {:keys [provider] :as data}]
   (let [llama3   (if (re-matches #"(?i).*llama.*3.*" (data :current_model)) true false)
         pre      {:model               (provider "model")
                   :temperature         (data :temperature)
                   :max_tokens          (data :max_tokens)
                   :seed                (data :seed)
                   :skip_special_tokens (if llama3 false true)
                   :messages            [{:role    "user"
                                          :content (data :prompt_final)}]}
         out      (if llama3 (merge pre {:stop ["<|eot_id|>"]}) pre)
         response (response-for (data :client) :chat-completions {:body out})
         status   (:status response)]
     (when (= status 200)
       (tap> {:from :chat-completion :env env :data data :out out})
       response))))

(defn completion
  "Generate prompt completion for a query.
  Params:
   - prompt: The text prompt to send
   Returns:
   - map of the query response"
  ([data] (completion nil data))
  ([_ data]
   (let [out {:model       (get-in data [:config "generic_model"])
              :temperature (get-in data [:config "temperature"])
              :max_tokens  (get-in data [:config "max_tokens"])
              :prompt      (get-in data [:opts :queries])}
         response (response-for (:client data) :completions {:body out})
         status (:status response)]
     (when (= status 200)
       response))))

(defn model-list
  "Get the available model list
   Returns:
   - A vector of available model names."
  ([data] (model-list nil data))
  ([_ data] (let [response (response-for (data :client) :internal-model-list)
                  status   (response :status)]
              (when (= status 200)
                (get-in response [:body :model_names])))))

(defn current-model
  "Get the currently loaded model
   Returns:
   - A string of the currently loaded model name"
  ([data] (current-model nil data))
  ([_ data] (let [response (response-for (data :client) :internal-model-info)
                  status   (response :status)]
              (when (= status 200)
                (get-in response [:body :model_name])))))

(defn current-model?
  "Check if the current model matches the supplied model.
   Params:
   - model: The model name to check against the current model.
   Returns:
   - true if the current model matches the supplied model, false otherwise."
  ([model data]
   (= model (data :current_model))))

(defn load-model!
  "Load a model.
   Params:
   - model: The model name to load.
   Returns:
   - The response body if the model was loaded successfully."
  ([model data]
   ; short circuit to avoid reloading same model
   (when (not (current-model? model data))
     (let [out     {:model_name model
                    :args       {:loader    (get-in data [:provider "loader"])
                                 :gpu_split (get-in data [:provider "gpu_split"])}
                    :settings   {}}
           response (response-for (:client data) :internal-model-load {:body out})
           status   (:status response)]
       (when (= status 200)
         (:body response))))))

(defn unload-model!
  "Unload current model.
   Returns:
  - The response body."
  ([data] (let [response (response-for (data :client) :internal-model-unload {:body ""})
                status   (response :status)]
            (when (= status 200)
              (:body response)))))

(comment
  (def p (p/open))
  (add-tap #'p/submit)
  (let [m (create-api-client {:api_base_url "http://127.0.0.1:5000"
                              :api_key "ooba-key"
                              :api_org "ooba-org"})]
    (martian/response-for m :internal-model-list)))
