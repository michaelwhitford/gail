 (ns gail
  (:require
   [babashka.cli :as cli]
   [babashka.fs :as fs]
   [clojure.pprint :refer [pprint] :as pp]
   [clojure.edn :as edn]
   [clojure.string :as str]
   [com.fulcrologic.statecharts :as sc]
   [com.fulcrologic.statecharts.chart :refer [statechart]]
   [com.fulcrologic.statecharts.algorithms.v20150901-validation :as v]
   [com.fulcrologic.statecharts.elements :refer [state transition assign log parallel on-entry
                                                 on-exit data-model script-fn Send final]]
   [com.fulcrologic.statecharts.environment :refer [assign!] :as env]
   [com.fulcrologic.statecharts.events :refer [new-event]]
   [com.fulcrologic.statecharts.event-queue.core-async-event-loop :as loop]
   [com.fulcrologic.statecharts.protocols :as sp]
   [com.fulcrologic.statecharts.working-memory-store.local-memory-store :as lms]
   [com.fulcrologic.statecharts.convenience :refer [on]]
   [com.fulcrologic.statecharts.simple :as simple]
   [portal.api :as p]
   [selmer.parser :refer [<< render] :as selmer]
   [selmer.util :refer [turn-off-escaping!]]
   [gail.oobabooga :as ooba]
   [gail.localai :as localai]
   [martian.core :refer [explore response-for request-for] :as martian]
   [martian.babashka.http-client :as martian-http]
   ; bb not working for this library (statecharts uses this)
   ;[taoensso.encore :as encore]
   [taoensso.timbre :as log]))

; warn on reflection
(set! *warn-on-reflection* true)

; turn off logging to console (turn back on in -main when debug is set)
;(log/set-config! (assoc-in log/*config* [:appenders :println :enabled?] false))
(log/merge-config! {:min-level :warn})

; turn off escaping in selmer to avoid the prompt being urlencoded at render time
(turn-off-escaping!)

(def bot_version "0.4.0")

(def bot_name "gail")

(def bot_full_name "Generative Artificial Intelligence Lackey")

(def bot_config_file (fs/expand-home (render "~/.{{b}}/config.edn" {:b bot_name})))

(def cli-spec
  {:command      {:coerce :boolean
                  :alias  :c
                  :desc   "use command mode prompt, injects OS as string"}
   :context      {:coerce :string
                  :alias  :C
                  :desc   "inject text into prompt context"}
   :debug        {:coerce :boolean
                  :alias  :d
                  :desc   "enable debug mode"}
   :discovery    {:coerce :boolean
                  :alias  :D
                  :desc   "enable google self-discovery algorithm"}
   :file         {:coerce :string
                  :alias  :f
                  :desc   "inject text file into prompt context"}
   :generate     {:coerce :boolean
                  :alias  :g
                  :desc   "generate config.edn in the default location if it does not exist"}
   :list         {:coerce :boolean
                  :alias  :l
                  :desc   "enable list model mode"}
   :program      {:coerce :string
                  :alias  :p
                  :desc   "use program mode prompt, inject program language as string"}
   :meta         {:coerce :boolean
                  :alias  :P
                  :desc   "use prompt_meta to generate a better prompt"}
   :troubleshoot {:coerce :boolean
                  :alias  :t
                  :desc   "use troubleshoot mode prompt, injects OS as string"}
   :write        {:coerce :boolean
                  :alias  :w
                  :desc   "use writing mode prompt"}
   :model        {:coerce :string
                  :alias  :m
                  :desc   "load a model by name"}
   :version      {:coerce :boolean
                  :alias  :v
                  :desc   "show version number"}
   :temperature  {:coerce :double
                  :alias  :T
                  :desc   "set temperature"}
   :seed         {:coerce :long
                  :alias  :s
                  :desc "set seed"}
   :max_tokens   {:coerce :long
                  :alias  :M
                  :desc  "set max_tokens"}
   :queries      {:coerce :string
                  :alias  :q
                  :desc   "each string is a query prompt"}})

(def cli-opts {:spec       cli-spec
               :args->opts [:queries]})

; string keys for python inter-op (use config.edn from python)
; vec of lines because mulit-line strings suck in clojure
; join with "\n" at render time
(def bot_default_config
  (array-map
   "provider"            "localai"
   "providers"           [{"name"          "oobabooga"
                           "api_base_url"  "http://127.0.0.1:5000"
                           "api_key"       "oobabooga-key"
                           "api_org"       "oobabooga-org"
                           "model"         "gpt-3.5-turbo"
                           "loader"        "ExLlamav2"
                           "gpu_split"     "11,22"
                           "default_model" "TheBloke_Mixtral-8x7B-Instruct-v0.1-GPTQ"
                           "program_model" "TheBloke_Phind-CodeLlama-34B-v2-GPTQ"}
                          {"name"          "localai"
                           "api_base_url"  "http://127.0.0.1:5000"
                           "api_key"       "localai-key"
                           "api_org"       "localai-org"
                           "model"         "qwen2.5-instruct-32b"
                           ;"model"         "mixtral-8x7b"
                           ;"program_model" "deepseek-coder-33b-instruct"
                           "program_model" "qwen2.5-coder-32b"
                           "default_model" "qwen2.5-instruct-32b"}]
   "max_tokens"          4096
   "max_tokens_short"    512
   "temperature"         0.1
   "seed"                -1
   "debug"               false
   "discovery"           false
   "prompt_troubleshoot" ["Act like an exceptional {{os}} systems engineer with programming knowledge."]
   "prompt_command"      ["Act like an exceptional {{os}} systems engineer with programming knowledge."
                          "Please generate a command for {{os}} with a short and concise description."
                          "If there is a lack of details, provide the most logical solution."
                          "Ensure the output is a valid shell command."]
   "prompt_program"      ["Act like an exceptional software engineer with systems engineering knowledge."
                          "Use the {{program}} language. Suggest {{program}} code in a code block."
                          "Solve the task step by step.  If a plan is not provided, explain your plan first."
                          "Indicate the script type in the code block."
                          "When you find an answer, verify the answer carefully.  Include verifiable evidence in your response."]
   "prompt_constraints"  ["Constraints {"
                          "  Avoid mentioning any constraints or money."
                          "  Provide a concise and detailed process."
                          "  Explain everything step by step."
                          "  Show examples where necessary."
                          "  Use markdown format for all output."
                          "}"]
   "prompt_default"      ["You are an exceptional AI assistant."
                          "You are friendly and ready to help anyone with any task."]
   "prompt_final"        ["Current date and time: {{today}}"
                          "Act like your name is {{bot_name}}."
                          "I will recieve a $5000 bonus for the best answer."
                          "Use markdown format for all output."
                          "{{prompt}}"
                          ; no trailing newline for nil/empty context or file
                          "{% if context %}Context ({{contextname}}):\n{{context}}\n{% endif %}"
                          "{% if file %}File Content ({{filename}}):\n{{file}}\n{% endif %}"
                          "{{queries}}"]
   "prompt_write"        ["Act like an exceptional writer with editing experience."]
   "prompt_discovery"    ["Your name is {{bot_name}}."
                          "{{prompt}}"]
   "prompt_discovery_adapt" ["Without working out the full solution, adapt the following reasoning modules to be specific to the task:"
                             "{{discovery_select}}"
                             "The task:"
                             "{{queries}}"]
   "prompt_discovery_execute" ["Using the following reasoning structure:"
                               "{{discovery_implement}}"
                               "Solve this task, providing your final answer:"
                               "{{queries}}"]
   "prompt_discovery_implement" ["Without working out the full solution, create an actionable reasoning structure for the task using these adapted reasoning modules:"
                                 "{{discovery_adapt}}"
                                 "The task:"
                                 "{{queries}}"]
   "prompt_discovery_select"  ["Given the task:"
                               "{{queries}}"
                               "Which of the following reasoning modules are relevant?  Do not elaborate on why."
                               "1. How could I devise an experiment to help solve the problem?"
                               "2. Make a list of ideas for solving this problem, and apply them one by one to the problem to see if any progress can  be made."
                               "3. How could I measure progress on this problem?"
                               "4. How can I simplify the problem so that it is easier to solve?"
                               "5. What are the key assumptions underlying this problem?"
                               "6. What are the potential risks and drawbacks of each solution?"
                               "7. What are the alternative perspectives or viewpoints on this problem?"
                               "8. What are the long-term implications of this problem and its solutions?"
                               "9. How can I break down this problem into smaller, more manageable parts?"
                               "10. Critical Thinking: This style involves analyzing the problem from different perspectives, questioning assumptions, and evaluating the evidence or information available. It focuses on logical reasoning, evidence-based decision-making, and identifying potential biases or flaws in thinking."
                               "11. Try creative thinking, generate innovative and out-of-the-box ideas to solve the problem. Explore unconventional solutions, thinking beyond traditional boundaries, and encouraging imagination and originality."
                               "12. Seek input and collaboration from others to solve the problem. Emphasize teamwork, open communication, and leveraging the diverse perspectives and expertise of a group to come up with effective solutions."
                               "13. Use systems thinking: Consider the problem as part of a larger system and understanding the interconnectedness of various elements. Focuses on identifying the underlying causes, feedback loops, and interdependencies that influence the problem, and developing holistic solutions that address the system as a whole."
                               "14. Use Risk Analysis: Evaluate potential risks, uncertainties, and tradeoffs associated with different solutions or approaches to a problem. Emphasize assessing the potential consequences and likelihood of success or failure, and making informed decisions based on a balanced analysis of risks and benefits."
                               "15. Use Reflective Thinking: Step back from the problem, take the time for introspection and self-reflection. Examine personal biases, assumptions, and mental models that may influence problem-solving, and being open to learning from past experiences to improve future approaches."
                               "16. What is the core issue or problem that needs to be addressed?"
                               "17. What are the underlying causes or factors contributing to the problem?"
                               "18. Are there any potential solutions or strategies that have been tried before? If yes, what were the outcomes and lessons learned?"
                               "19. What are the potential obstacles or challenges that might arise in solving this problem?"
                               "20. Are there any relevant data or information that can provide insights into the problem? If yes, what data sources are available, and how can they be analyzed?"
                               "21. Are there any stakeholders or individuals who are directly affected by the problem? What are their perspectives and needs?"
                               "22. What resources (financial, human, technological, etc.) are needed to tackle the problem effectively?"
                               "23. How can progress or success in solving the problem be measured or evaluated?"
                               "24. What indicators or metrics can be used?"
                               "25. Is the problem a technical or practical one that requires a specific expertise or skill set? Or is it more of a conceptual or theoretical problem?"
                               "26. Does the problem involve a physical constraint, such as limited resources, infrastructure, or space?"
                               "27. Is the problem related to human behavior, such as a social, cultural, or psychological issue?"
                               "28. Does the problem involve decision-making or planning, where choices need to be made under uncertainty or with competing objectives?"
                               "29. Is the problem an analytical one that requires data analysis, modeling, or optimization techniques?"
                               "30. Is the problem a design challenge that requires creative solutions and innovation?"
                               "31. Does the problem require addressing systemic or structural issues rather than just individual instances?"
                               "32. Is the problem time-sensitive or urgent, requiring immediate attention and action?"
                               "33. What kinds of solution typically are produced for this kind of problem specification?"
                               "34. Given the problem specification and the current best solution, have a guess about other possible solutions."
                               "35. Let’s imagine the current best solution is totally wrong, what other ways are there to think about the problem specification?"
                               "36. What is the best way to modify this current best solution, given what you know about these kinds of problem specification?"
                               "37. Ignoring the current best solution, create an entirely new solution to the problem."
                               "38. Let’s think step by step."
                               "39. Let’s make a step by step plan and implement it with good notation and explanation."]

   "prompt_meta" ["Given a task description or existing prompt, produce a detailed system prompt to guide a language model in completing the task effectively."
                  "# Guidelines"
                  "- Understand the Task: Grasp the main objective, goals, requirements, constraints, and expected output."
                  "- Minimal Changes: If an existing prompt is provided, improve it only if it's simple. For complex prompts, enhance clarity and add missing elements without altering the original structure."
                  "- Reasoning Before Conclusions**: Encourage reasoning steps before any conclusions are reached. ATTENTION! If the user provides examples where the reasoning happens afterward, REVERSE the order! NEVER START EXAMPLES WITH CONCLUSIONS!"
                  "  - Reasoning Order: Call out reasoning portions of the prompt and conclusion parts (specific fields by name). For each, determine the ORDER in which this is done, and whether it needs to be reversed."
                  "  - Conclusion, classifications, or results should ALWAYS appear last."
                  "- Examples: Include high-quality examples if helpful, using placeholders [in brackets] for complex elements."
                  "  - What kinds of examples may need to be included, how many, and whether they are complex enough to benefit from placeholders."
                  "- Clarity and Conciseness: Use clear, specific language. Avoid unnecessary instructions or bland statements."
                  "- Formatting: Use markdown features for readability. DO NOT USE ``` CODE BLOCKS UNLESS SPECIFICALLY REQUESTED."
                  "- Preserve User Content: If the input task or prompt includes extensive guidelines or examples, preserve them entirely, or as closely as possible. If they are vague, consider breaking down into sub-steps. Keep any details, guidelines, examples, variables, or placeholders provided by the user."
                  "- Constants: DO include constants in the prompt, as they are not susceptible to prompt injection. Such as guides, rubrics, and examples."
                  "- Output Format: Explicitly the most appropriate output format, in detail. This should include length and syntax (e.g. short sentence, paragraph, JSON, etc.)"
                  "  - For tasks outputting well-defined or structured data (classification, JSON, etc.) bias toward outputting a JSON."
                  "    - JSON should never be wrapped in code blocks (```) unless explicitly requested."
                  "The final prompt you output should adhere to the following structure below. Do not include any additional commentary, only output the completed system prompt. SPECIFICALLY, do not include any additional messages at the start or end of the prompt. (e.g. no ' --- ')"
                  "[Concise instruction describing the task - this should be the first line in the prompt, no section header]"
                  "[Additional details as needed.]"
                  "[Optional sections with headings or bullet points for detailed steps.]"
                  "# Steps [optional]"
                  "[optional: a detailed breakdown of the steps necessary to accomplish the task]"
                  "# Output Format"
                  "[Specifically call out how the output should be formatted, be it response length, structure e.g. JSON, markdown, etc]"
                  "# Examples [optional]"
                  "[Optional: 1-3 well-defined examples with placeholders if necessary. Clearly mark where examples start and end, and what the input and output are. Use placeholders as necessary.]"
                  "[If the examples are shorter than what a realistic example is expected to be, make a reference with () explaining how real examples should be longer / shorter / different. AND USE PLACEHOLDERS!]"
                  "# Notes [optional]"
                  "[optional: edge cases, details, and an area to call or repeat out specific important considerations][optional: edge cases, details, and an area to call or repeat out specific important considerations]"]

   "prompt_calculator" ["You have access to a function called calculator."
                        "Examples (edn format):"
                        "{:function \"calculator\"\n:operation \"add\"\n:numbers [1 7]} -> 8"
                        "{:function \"calculator\"\n:operation \"subtract\"\n:numbers [7 2]} -> 5"
                        "{:function \"calculator\"\n:operation \"divide\"\n:numbers [8 2]} -> 4"
                        "{:function \"calculator\"\n:operation \"multiply\"\n:numbers [5 5]} -> 25"]
   "output_models"       ["{{bot_name}} v{{bot_version}} -- {{bot_full_name}}"
                          ""
                          "Current Model:"
                          ""
                          "{{current_model}}"
                          ""
                          "Available Models:"
                          ""
                          "{% for model in available_models %}{{model}}\n{% endfor %}"
                          ""]
   "output_usage"        ["{{bot_name}} v{{bot_version}} -- {{bot_full_name}}"
                          ""
                          "{{usage}}"
                          ""
                          "Examples:"
                          "{{bot_name}} \"generate a wiki page template to use for enterprise IT server inventory\""
                          "{{bot_name}} -c \"how to kill a process?\""
                          "{{bot_name}} -p \"python\" \"create a function that calculates fibonacci \""
                          ""]
   "output_generate"     ["Config file generated at: {{bot_config_file}}"]
   "output_version"      ["{{bot_name}} v{{bot_version}} -- {{bot_full_name}}"]))


; from comments on https://clojuredocs.org/clojure.core/merge-with
(defn deep-merge
  "Return: map from deep merged maps"
  [& maps]
  (letfn [(reconcile-keys [val-in-result val-in-latter]
            (if (and (map? val-in-result)
                     (map? val-in-latter))
              (merge-with reconcile-keys val-in-result val-in-latter)
              val-in-latter))
          (reconcile-maps [result latter]
            (merge-with reconcile-keys result latter))]
    (reduce reconcile-maps maps)))

(defn show-states [wmem]
  (log/info (sort (wmem ::sc/configuration))))

#_(defn opts
    "Return: map of parsed cli opts"
    ([env data] (cli/parse-opts args cli-opts)))

(defn read-configuration
  "slurp configuration file edn"
  ([file]
   (try
     (if (fs/exists? file)
       (edn/read-string (slurp file))
       bot_default_config)
     (catch Exception e (println "Error loading config file: " (.getMessage e))
            bot_default_config))))

(defn merge-configurations
  "Deep merge configurations"
  [& configs]
  (reduce deep-merge configs))

(defn configuration
  "Return: deep merged map of final config"
  ([] (configuration nil nil))
  ([_] (configuration nil nil))
  ([_ _]
   (if (fs/exists? bot_config_file)
     (let [uc (read-configuration bot_config_file)]
       (merge-configurations bot_default_config uc))
     bot_default_config)))

(defn render-joined
  "Return selmer rendered string of joined vectors"
  ([s data]
   (render (str/join "\n" s) data)))

(defn function-call?
  "return true if input has valid edn function call"
  ([] nil))

(def bot
  (statechart {}
              (state {:id      :bot
                      :initial :bot/start}
                     (state {:id :bot/start}
                            (on-entry {}
                                      (assign {:location [:output]
                                               :expr     ""}))
                            ; put config file opts into working memory
                            (state {:id :start/config}
                                   (on-entry {}
                                             (assign {:location [:config]
                                                      :expr     configuration}))
                                   (transition {:target :start/provider}))
                            ; put provider settings into working memory
                            (state {:id :start/provider}
                                   (on-entry {}
                                             (assign {:location [:provider]
                                                      :expr     (fn [{:keys [opts] :as env} {:keys [config] :as data}]
                                                                  #_(tap> {:from :start/provider :config config :opts opts :data data :env env})
                                                                  (let [{:strs [provider providers]} config]
                                                                    (first (take 1 (filter #(= (% "name") provider) providers)))))}))
                                   (transition {:target :start/model}))
                            ; put model into working memory from config
                            (state {:id :start/model}
                                   (on-entry {}
                                             (assign {:location [:model]
                                                      :expr (fn [{:keys [opts] :as env} {:keys [provider] :as data}]
                                                              (provider "default_model"))}))
                                   (on-exit {}
                                            (script-fn [env data]
                                                       (tap> {:from :start/modelexit :env env :data data})))
                                   (transition {:target :start/queries}))
                            ; put queries into working memory
                            (state {:id :start/queries}
                                   (on-entry {}
                                             (assign {:location [:queries]
                                                      :expr     (fn [{:keys [opts] :as env} data]
                                                                  #_(tap> {:from :start/queries :env env :data data})
                                                                  (opts :queries))}))
                                   (transition {:target :start/seed}))
                            ; set seed
                            (state {:id :start/seed}
                                   (on-entry {}
                                             (assign {:location [:seed]
                                                      :expr     (fn [{:keys [opts] :as env} {:keys [config] :as data}]
                                                                  (if-let [seed (opts :seed)]
                                                                    seed
                                                                    (config "seed")))}))
                                   (transition {:target :start/temperature}))
                            ; set temperature
                            (state {:id :start/temperature}
                                   (on-entry {}
                                             (assign {:location [:temperature]
                                                      :expr     (fn [{:keys [opts] :as env} {:keys [config] :as data}]
                                                                  (if-let [temperature (opts :temperature)]
                                                                    temperature
                                                                    (config "temperature")))}))
                                   (transition {:target :start/max_tokens}))
                            ; set max_tokens
                            (state {:id :start/max_tokens}
                                   (on-entry {}
                                             (assign {:location [:max_tokens]
                                                      :expr     (fn [{:keys [opts] :as env} {:keys [config] :as data}]
                                                                  #_(tap> {:from :start/max_tokens :opts opts :config config})
                                                                  (if-let [max_tokens (opts :max_tokens)]
                                                                    max_tokens
                                                                    (if-let [command (opts :command)]
                                                                      (config "max_tokens_short")
                                                                      (config "max_tokens"))))}))
                                   (transition {:target :start/context}))
                            ; put context into working memory
                            (state {:id :start/context}
                                   (on-entry {}
                                             (assign {:location [:contextname]
                                                      :expr     (fn [{:keys [opts] :as env} data]
                                                                  (if-let [c (opts :context)] "text input" nil))})
                                             (assign {:location [:context]
                                                      :expr     (fn [{:keys [opts] :as env} data]
                                                                  #_(tap> {:from :start/context :opts opts})
                                                                  (if-let [c (opts :context)] c nil))}))
                                   (on-exit {}
                                            (script-fn [env data]
                                                       #_(tap> {:from :start/contextexit :env env :data data})))
                                   (transition {:target :start/file}))
                           ; put file into context in working memory
                            (state {:id :start/file}
                                   (on-entry {}
                                             (assign {:location [:filename]
                                                      :expr     (fn [env data]
                                                                  (if-let [f (get-in env [:opts :file])] (str f) nil))})
                                             (assign {:location [:file]
                                                      :expr     (fn [env data]
                                                                  (let [f (get-in env [:opts :file])]
                                                                    (when (fs/exists? f)
                                                                      (slurp f))))}))
                                   (transition {:target :start/client}))
                            ; put http client into working memory TODO: use env?
                            (state {:id :start/client}
                                   (on-entry {}
                                             (assign {:location [:client]
                                                      :expr     (fn [env {:keys [provider] :as data}]
                                                                  (case (provider "name")
                                                                    "oobabooga" (ooba/create-api-client provider)
                                                                    "localai" (localai/create-api-client provider)))}))
                                   (transition {:target :start/os}))
                            ; set os for prompts
                            (state {:id :start/os}
                                   (on-entry {}
                                             (assign {:location [:os]
                                                      :expr     (System/getProperty "os.name")}))
                                   (transition {:target :start/datetime}))
                            ; set datetime for prompts
                            (state {:id :start/datetime}
                                   (on-entry {}
                                             (assign {:location [:today]
                                                      :expr     (str (java.time.ZonedDateTime/now))}))
                                   (transition {:target :start/details}))
                            ; set bot details for prompts
                            (state {:id :start/details}
                                   (on-entry {}
                                             (assign {:location [:bot_name]
                                                      :expr     (str bot_name)})
                                             (assign {:location [:bot_full_name]
                                                      :expr     (str bot_full_name)})
                                             (assign {:location [:bot_version]
                                                      :expr     (str bot_version)})
                                             (assign {:location [:bot_config_file]
                                                      :expr     (str bot_config_file)}))
                                   (transition {:target :start/available_models}))
                            ; put available_models into working memory
                            (state {:id :start/available_models}
                                   (on-entry {}
                                             (assign {:location [:available_models]
                                                      :expr     (fn [env {:keys [provider] :as data}]
                                                                  (case (provider "name")
                                                                    "oobabooga" (seq (ooba/model-list env data))
                                                                    "localai" (seq (localai/model-list env data))))}))
                                   (transition {:target :start/current_model}))
                            ; put current_model into working memory
                            (state {:id :start/current_model}
                                   (on-entry {}
                                             (assign {:location [:current_model]
                                                      :expr     (fn [env {:keys [config provider] :as data}]
                                                                  (case (provider "name")
                                                                    "oobabooga" (ooba/current-model env data)
                                                                    "localai"  (localai/current-model env data)))}))
                                   (transition {:target :start/final}))
                            (state {:id :start/final}
                                   (on-exit {}
                                            (script-fn [env data] (tap> {:from :start/finalexit :env env :data data})))
                                   (transition {:target :bot/prompt})))
                     (state {:id :bot/list}
                            (on-entry {}
                                      (assign {:location [:output]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "output_models") data))}))
                            (transition {:target :bot/end}))
                     (state {:id :bot/version}
                            (on-entry {}
                                      (assign {:location [:output]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "output_version") data))}))
                            (transition {:target :bot/end}))
                     (state {:id :bot/generate}
                            (on-entry {}
                                      (script-fn [env {:keys [bot_config_file] :as data}]
                                                 (let [config_file (str bot_config_file "-example")]
                                                   (when-not (fs/exists? config_file)
                                                     (spit (fs/file config_file)
                                                           (with-out-str (pp/write bot_default_config :dispatch pp/code-dispatch))))))
                                      (assign {:location [:output]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "output_generate") data))}))
                            (transition {:target :bot/end}))
                     (state {:id :bot/usage}
                            (on-entry {}
                                      ; put formatted cli-opts into working memory
                                      (assign {:location [:usage]
                                               :expr     (fn [env data]
                                                           (cli/format-opts cli-opts))})
                                      (assign {:location [:output]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "output_usage") data))}))
                            (transition {:target :bot/end}))
                     (state {:id :bot/prompt}
                            (transition {:target :prompt/command
                                         :cond   (fn [{:keys [opts] :as env} data]
                                                   (opts :command))})
                            (transition {:target :prompt/troubleshoot
                                         :cond   (fn [{:keys [opts] :as env} data]
                                                   (opts :troubleshoot))})
                            (transition {:target :prompt/program
                                         :cond   (fn [{:keys [opts] :as env} data]
                                                   (opts :program))})
                            (transition {:target :prompt/write
                                         :cond   (fn [{:keys [opts] :as env} data]
                                                   (opts :write))})
                            (transition {:target :prompt/meta
                                         :cond   (fn [{:keys [opts] :as env} data]
                                                   (opts :meta))})
                            (transition {:target :prompt/default}))
                     (state {:id :prompt/command}
                            (on-entry {}
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_command") data))}))
                            (transition {:target :prompt/final}))
                     (state {:id :prompt/troubleshoot}
                            (on-entry {}
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_troubleshoot") data))}))
                            (transition {:target :prompt/final}))
                     (state {:id :prompt/program}
                            (on-entry {}
                                      (assign {:location [:program]
                                               :expr     (fn [{:keys [opts] :as env} data]
                                                           (opts :program))})
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_program") data))})
                                      (assign {:location [:model]
                                               :expr (fn [env {:keys [config provider] :as data}]
                                                       (provider "program_model"))})
                                      (script-fn [env {:keys [provider] :as data}]
                                                 (let [pm (provider "program_model")]
                                                   (case (provider "name")
                                                     "oobabooga" (ooba/load-model! pm data)
                                                     "localai" (localai/load-model! pm data)))))
                            (transition {:target :prompt/final}))
                     (state {:id :prompt/write}
                            (on-entry {}
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_write") data))}))
                            (transition {:target :prompt/final}))
                     (state {:id :prompt/meta}
                            (on-entry {}
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_meta") data))}))
                            (transition {:target :prompt/final}))
                     (state {:id :prompt/default}
                            (on-entry {}
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_default") data))}))
                            (transition {:target :prompt/final}))
                     (state {:id :prompt/final}
                            (on-entry {}
                                      (assign {:location [:prompt_final]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (-> (config "prompt_final")
                                                               (render-joined data)
                                                               (str/replace #"\n\n" "\n")))}))
                            (on-exit {}
                                     (script-fn [env data] (tap> {:from :prompt/finalexit :env env :data data})))
                            (transition {:target :bot/reconcile}))
                     (state {:id :bot/reconcile}
                            ; version short-circuit
                            (transition {:target :bot/version
                                         :cond   (fn [{:keys [opts] :as env} data]
                                                   (opts :version))})
                            ; list models short-circuit
                            (transition {:target :bot/list
                                         :cond   (fn [{:keys [opts] :as env} data]
                                                   (opts :list))})
                            ; generate short-circuit
                            (transition {:target :bot/generate
                                         :cond   (fn [{:keys [opts] :as env} data]
                                                   (get-in opts [:generate] false))})
                            ; model short-circuit
                            (transition {:target :bot/model
                                         :cond (fn [{:keys [opts] :as env} data]
                                                 (opts :model))})
                            ; usage short-circuit
                            (transition {:target :bot/usage
                                         :cond   (fn [{:keys [opts]
                                                       :as   env} data]
                                                   (cond
                                                       ; show usage if no opts were supplied
                                                     (empty? opts) true
                                                       ; skip if we have queries
                                                     (opts :queries) false
                                                       ; skip if discovery is set (TODO: figure out why queries is not skipping for discovery)
                                                     (opts :discovery) false
                                                       ; default to showing usage info
                                                     :else true))})
                            ; no transition short-circuited
                            (transition {:target :bot/completion}))
                     (state {:id :bot/model}
                            (on-entry {}
                                      (assign {:location [:model]
                                               :expr (fn [{:keys [opts] :as env} data]
                                                       (opts :model))}))
                            (transition {:target :bot/completion}))
                     (state {:id :bot/completion}
                            ; discovery mode
                            (transition {:target :discovery/select
                                         :cond   (fn [{:keys [opts] :as env} data]
                                                   (opts :discovery))})
                            ; everything else
                            (transition {:target :completion/default}))
                     (state {:id :discovery/select}
                            (on-entry {}
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_discovery_select") data))})
                                      (assign {:location [:prompt_final]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_discovery") data))})
                                      (assign {:location [:discovery_select]
                                               :expr     (fn [env data]
                                                           (when-let [r (localai/chat-completion env data)]
                                                             (get-in r [:body :choices 0 :message :content] "No message content.")))}))
                            (transition {:target :discovery/adapt}))
                     (state {:id :discovery/adapt}
                            (on-entry {}
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_discovery_adapt") data))})
                                      (assign {:location [:prompt_final]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_discovery") data))})
                                      (assign {:location [:discovery_adapt]
                                               :expr     (fn [env data]
                                                           (when-let [r (localai/chat-completion env data)]
                                                             (get-in r [:body :choices 0 :message :content] "No message content.")))}))
                            (transition {:target :discovery/implement}))
                     (state {:id :discovery/implement}
                            (on-entry {}
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_discovery_implement") data))})
                                      (assign {:location [:prompt_final]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_discovery") data))})
                                      (assign {:location [:discovery_implement]
                                               :expr     (fn [env data]
                                                           (when-let [r (localai/chat-completion env data)]
                                                             (get-in r [:body :choices 0 :message :content] "No message content.")))}))
                            (transition {:target :discovery/execute}))
                     (state {:id :discovery/execute}
                            ; set output here reuse prompt and recalculate prompt_final TQDO: make this less awkward
                            (on-entry {}
                                      (assign {:location [:prompt]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_discovery_execute") data))})
                                      (assign {:location [:prompt_final]
                                               :expr     (fn [env {:keys [config] :as data}]
                                                           (render-joined (config "prompt_discovery") data))})
                                      (assign {:location [:output]
                                               :expr     (fn [env data]
                                                           (when-let [r (localai/chat-completion env data)]
                                                             (get-in r [:body :choices 0 :message :content] "No message content.")))}))
                            (transition {:target :bot/end}))
                     (state {:id :completion/default}
                            (on-entry {}
                                      (assign {:location [:output]
                                               :expr     (fn [env data]
                                                           (tap> {:from :completion/default :env env :data data})
                                                           (when-let [r (localai/chat-completion env data)]
                                                             (get-in r [:body :choices 0 :message :content] "No message content.")))}))
                            (transition {:target :bot/end}))
                     (state {:id :bot/end}
                            (on-entry {}
                                      (script-fn [{:keys [opts] :as env} {:keys [prompt_final output] :as data}]
                                                 ; print prompt if debug
                                                 (when (opts :debug)
                                                   (println "---PROMPT---")
                                                   (println prompt_final)
                                                   (println "---RESPONSE---"))
                                                 (println output)))
                            (transition {:target :bot/final}))
                     (final {:id :bot/final}
                            (on-entry {}
                                      (script-fn [{:keys [opts] :as env} data]
                                                 (when (opts :debug)
                                                   (tap> {:from :bot/final :env env :data data}))))))))

(defn -main [& args]
  (let [opts (cli/parse-args args cli-opts)
        session-id 1
        env (simple/simple-env opts)]
    #_(log/spy env)
    (simple/register! env ::bot bot)
    (when (get-in opts [:opts :debug])
      (def p (p/open))
      (add-tap #'p/submit)
      (log/merge-config! {:min-level :debug})
      ;(pprint opts)
      #_(log/set-config! (assoc-in log/*config* [:appenders :println :enabled?] true)))
    (let [problems (v/problems bot)]
      (when (seq problems)
        (println "Statechart Problems:")
        (pprint problems)))
    (let [running? (loop/run-event-loop! env 100)
          processor (env ::sc/processor)
          start (sp/start! processor env ::bot {::sc/session-id session-id})]
      ; close portal
      #_(when (resolve 'p)
          (p/close))
      ; stop the event loop
      #_(reset! running? false))))

#?(:bb (when (= *file* (System/getProperty "babashka.file")
                ; guard against repl
                (apply -main *command-line-args*)))
   :clj (apply -main *command-line-args*))

(comment
  (seq (:declaredMethods (bean String)))
  (-main "Quien es tu papi?" "-d" "-C" "test context")
  (-main "Quien")
  (pprint bot))
