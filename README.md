# gail

**G**enerative **AI** **L**ackey

**lackey** _'la-kē_ **noun**

1. someone who does menial tasks or runs errands for another
2. a servile follower

gail is your AI lackey and can help you from the command line.

poorly written clojure, runs from clojure or babashka.

slightly coupled to localai as the api server.

If you are running localai, and want to use AI from the cli, gail might be just what you need.

This works very well for me to interact with local AI from the cli.

A tool for 3 people in the whole world.  I am One of Three, Are you Two of Three?

## Pre-requisite Installs

* [Install LocalAI](https://github.com/mudler/LocalAI)
* [Install Babashka](https://github.com/babashka/babashka) or [Install Clojure](https://clojure.org/guides/install_clojure)

clone the repository

```
git clone https://gitlab.com/michaelwhitford/gail.git
```

symlink the gail shell script into your path

```
ln -s /path/to/gail/gail /path/to/bin/gail
```

**or** add the git repo directory to the path

```
echo 'export PATH="${PATH}:/path/src/gail"' > ~/.bashrc
```

## Examples

### General Mode

```
gail "generate a wiki page template to use for enterprise IT server inventory."
```

### Troubleshooting Mode

Here we turn on troubleshooting mode with -t, and inject text into the prompt context using output from grep. The OS should be injected into the prompt automatically.

```
gail -t "username can't login" -C "$(grep username /var/log/auth.log)"
```

### Command Mode

Here we turn on command mode with -c, the os should be injected into the prompt automatically.

```
gail -c "find all files over 100M in size"
```

Here we turn on command mode, and inject the output from the ps command into the prompt context.

```
gail -c -C "$(ps aux)" "How to kill the process using the most memory?"
```

### Programming Mode

Here we set the programming language, and ask for code output.

```
gail -p "python" "Generate a function to calculate fibonacci."
```

Here we set the programming language, and inject a text file into the prompt context.

```
gail -p "python" -f /path/to/script.py "Write unit tests for the script from the context."
```

### Writing Mode

Here we turn on writing mode, and set a higher temperature (more creative).
```
gail -w -T 0.7 "Create a poem about gerbils and alligators in the style of Lewis Carroll"
```

### Debug Mode

Add -d to turn on debug mode

```
gail "What time is it?" -d
```
